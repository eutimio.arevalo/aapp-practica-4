#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

void entradas(int proceso, int *n);
int busqueda(int proceso,int *matriz, int columnas, int filas, int numero);

int main(void) {
    int proceso, nro_procesos, numero;
    int contador = 0;
    int cont_total = 0;
    int *matriz = NULL;
    
    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &proceso);
    MPI_Comm_size(MPI_COMM_WORLD, &nro_procesos);
    entradas(proceso, &numero);

    int filas = 10000;
    int local_filas = filas / nro_procesos;
    int columnas = 10000;

    unsigned int semilla = (unsigned int)time(NULL);
    srand(semilla);
    
    matriz = (int *)malloc(filas * columnas * sizeof(int));

    for (int i = 0; i < filas; i++) {
        for (int j = 0; j < columnas; j++) {
            matriz[i * columnas + j] = rand() % (1000+1);
        }
    }

    contador = busqueda(proceso,matriz,columnas,local_filas,numero);  
    MPI_Reduce(&contador, &cont_total, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (proceso == 0) {
        printf("\n\nDentro de la matriz el numero %d aparece %d veces.\n", numero, cont_total);
    }
    
    free(matriz);    
    MPI_Finalize();
    
    return 0;
}

void entradas(int proceso, int *n)
{
    if (proceso == 0)
    {
        printf("Ingresa un valor\n");
        scanf("%d", n);
    }

    MPI_Bcast(n, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

int busqueda(int proceso,int *matriz, int columnas, int filas, int numero){
    int contador = 0;
    for (int i = (proceso*filas); i < filas*(proceso+1); i++) {
        for (int j = 0; j < columnas; j++) {
            //printf("%d\t",matriz[i * columnas + j]);
            if (matriz[i * columnas + j] == numero) {
                contador++;
            }
            //printf("\n");
        }
    }
    printf("Soy el proceso %d, y el número %d se repite %d veces\n",proceso,numero,contador);
    return contador;
}