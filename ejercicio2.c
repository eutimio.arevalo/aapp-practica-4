#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include <mpi.h>

void entradas(int proceso, int *n);
int *extraer_diagonal_principal(int *matriz, int filas, int columnas);
int *extraer_diagonal_secundaria(int *matriz, int filas, int columnas);
int sumar_diagonal(int *diagonal, int local_filas, int proceso, char *diagonal_nombre);
int encontrar_valor_alto(int *arreglo, int local_filas, int proceso, char *diagonal_nombre);
void decidir_valor(int valor_alto, int valor_alto2, bool suma_valMax);

int main(void)
{
    int proceso, nro_procesos, numero;

    int total_parcial, total, resultado;
    int valor_parcial_alto, valor_alto, valor_alto2;

    int *matriz = NULL;
    int *diagonal_principal = NULL;
    int *diagonal_secundaria = NULL;

    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &proceso);
    MPI_Comm_size(MPI_COMM_WORLD, &nro_procesos);

    int filas = 10000;
    int local_filas = filas / nro_procesos;
    int columnas = 10000;

    unsigned int semilla = (unsigned int)time(NULL);
    srand(semilla);

    matriz = (int *)malloc(filas * columnas * sizeof(int));

    for (int i = 0; i < filas; i++)
    {
        for (int j = 0; j < columnas; j++)
        {
            matriz[i * columnas + j] = rand() % (1000 + 1);
        }
    }

    diagonal_principal = extraer_diagonal_principal(matriz, filas, columnas);
    diagonal_secundaria = extraer_diagonal_secundaria(matriz, filas, columnas);
    free(matriz);

    while (numero != 0)
    {
        entradas(proceso, &numero);

        switch (numero)
        {
        case 1:
            /* SUMAR DIAGONAL PRINCIPAL */
            total = 0;
            total_parcial = sumar_diagonal(diagonal_principal, local_filas, proceso, "principal");
            MPI_Reduce(&total_parcial, &total, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
            if (proceso == 0)
            {
                printf("\nLa suma total de la diagonal principal es: %d\n\n", total);
            }
            break;

        case 2:
            /* SUMAR DIAGONAL SECUNDARIA */
            total = 0;
            total_parcial = sumar_diagonal(diagonal_secundaria, local_filas, proceso, "secundaria");
            MPI_Reduce(&total_parcial, &total, 1, MPI_INT, MPI_SUM, 1, MPI_COMM_WORLD);
            if (proceso == 1)
            {
                printf("\nLa suma total de la diagonal secundaria es: %d\n\n", total);
            }
            break;

        case 3:
            /* SUMAR AMBAS DIAGONALES */
            total = 0;
            total_parcial = sumar_diagonal(diagonal_principal, local_filas, proceso, "principal");
            MPI_Reduce(&total_parcial, &total, 1, MPI_INT, MPI_SUM, 1, MPI_COMM_WORLD);
            if (proceso == 1)
            {
                MPI_Send(&total, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
            }

            resultado = 0;
            total_parcial = sumar_diagonal(diagonal_secundaria, local_filas, proceso, "secundaria");
            MPI_Reduce(&total_parcial, &resultado, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
            if (proceso == 0)
            {
                MPI_Recv(&total, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                resultado += total;
                printf("\nLa suma total de las dos diagonales es: %d\n\n", resultado);
            }
            break;

        case 4:
            /* ENCONTRAR VALOR MAS ALTO EN DIAGONAL PRINCIPAL */
            valor_alto = 0;
            valor_parcial_alto = encontrar_valor_alto(diagonal_principal, local_filas, proceso, "principal");
            MPI_Reduce(&valor_parcial_alto, &valor_alto, 1, MPI_INT, MPI_MAX, 2, MPI_COMM_WORLD);
            if (proceso == 2)
            {
                printf("\nEl valor mas alto de la diagonal principal es: %d\n", valor_alto);
            }

            break;

        case 5:
            /* ENCONTRAR VALOR MAS ALTO EN DIAGONAL SECUNDARIA */
            valor_alto = 0;
            valor_parcial_alto = encontrar_valor_alto(diagonal_secundaria, local_filas, proceso, "secundaria");
            MPI_Reduce(&valor_parcial_alto, &valor_alto, 1, MPI_INT, MPI_MAX, 3, MPI_COMM_WORLD);
            if (proceso == 3)
            {
                printf("\nEl valor mas alto de la diagonal secundaria es: %d\n", valor_alto);
            }
            break;

        case 6:
            /* ENCONTRAR VALOR MAS ALTO EN AMBAS DIAGONALES */
            valor_alto = 0;
            valor_parcial_alto = encontrar_valor_alto(diagonal_principal, local_filas, proceso, "principal");
            MPI_Reduce(&valor_parcial_alto, &valor_alto, 1, MPI_INT, MPI_MAX, 3, MPI_COMM_WORLD);
            if (proceso == 3)
            {
                // printf("\nEl valor mas alto de la diagonal principal es: %d\n",valor_alto);
                MPI_Send(&valor_alto, 1, MPI_INT, 2, 0, MPI_COMM_WORLD);
            }

            valor_alto2 = 0;
            valor_parcial_alto = encontrar_valor_alto(diagonal_secundaria, local_filas, proceso, "secundaria");
            MPI_Reduce(&valor_parcial_alto, &valor_alto2, 1, MPI_INT, MPI_MAX, 2, MPI_COMM_WORLD);
            if (proceso == 2)
            {
                // printf("\nEstoy en el proceso %d\n", proceso);
                MPI_Recv(&valor_alto, 1, MPI_INT, 3, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                printf("\nValor más grande de la Diagonal Principal: %d\n", valor_alto);
                printf("Valor más grande de la Diagonal Secundaria: %d\n", valor_alto2);
                decidir_valor(valor_alto, valor_alto2,false);
                sleep(3);
            }
            break;

        case 7:
            /* DETERMINAR CUAL ES LA DIAGONAL MAS GRANDE */
            total = 0;
            total_parcial = sumar_diagonal(diagonal_principal, local_filas, proceso, "principal");
            MPI_Reduce(&total_parcial, &total, 1, MPI_INT, MPI_SUM, 1, MPI_COMM_WORLD);
            if (proceso == 1)
            {
                MPI_Send(&total, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
            }

            resultado = 0;
            total_parcial = sumar_diagonal(diagonal_secundaria, local_filas, proceso, "secundaria");
            MPI_Reduce(&total_parcial, &resultado, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
            if (proceso == 0)
            {
                MPI_Recv(&total, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                decidir_valor(total, resultado,true);
            }
            break;

        default:
            if(proceso==0){
                printf("\nGracias, Hasta Luego!!\n"); 
            }
            
            break;
        }
    }

    // contador = busqueda(proceso,matriz,columnas,local_filas,numero);
    // MPI_Reduce(&contador, &cont_total, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    // if (proceso == 0) {
    //     printf("\n\nDentro de la matriz el numero %d aparece %d veces.\n", numero, cont_total);
    // }
    free(diagonal_principal);
    free(diagonal_secundaria);
    MPI_Finalize();

    return 0;
}

void entradas(int proceso, int *n)
{
    if (proceso == 0)
    {
        sleep(1);
        printf("\n\nSeleccioné una de las siguientes opciones:\n");
        printf("\t 0) Salir\n");
        printf("\t 1) Calcular suma de la diagonal principal\n");
        printf("\t 2) Calcular suma de la diagonal secundaria\n");
        printf("\t 3) Calcular suma de ambas diagonales\n");
        printf("\t 4) Obtener el valor más alto de la diagonal principal\n");
        printf("\t 5) Obtener el valor más alto de la diagonal secundaria\n");
        printf("\t 6) Obtener el valor más alto de ambas diagonales\n");
        printf("\t 7) Determinar cual diagonal es más grande\n");

        scanf("%d", n);
    }

    MPI_Bcast(n, 1, MPI_INT, 0, MPI_COMM_WORLD);
}

int *extraer_diagonal_principal(int *matriz, int filas, int columnas)
{
    int *diagonal = (int *)malloc(filas * sizeof(int));
    for (int i = 0; i < filas; i++)
    {
        diagonal[i] = matriz[i * columnas + i];
    }
    return diagonal;
}

int *extraer_diagonal_secundaria(int *matriz, int filas, int columnas)
{
    int *diagonal = (int *)malloc(filas * sizeof(int));
    for (int i = 0; i < filas; i++)
    {
        diagonal[i] = matriz[(i + 1) * columnas - (i + 1)];
    }
    return diagonal;
}

int sumar_diagonal(int *diagonal, int local_filas, int proceso, char *diagonal_nombre)
{
    int total = 0;
    for (int i = (local_filas * proceso); i < local_filas * (proceso + 1); i++)
    {
        total = total + diagonal[i];
    }
    printf("La suma parcial de la diagonal %s en el proceso %d es %d\n", diagonal_nombre,proceso, total);
    return total;
}

int encontrar_valor_alto(int *arreglo, int local_filas, int proceso, char *diagonal_nombre)
{
    int valor = arreglo[0];
    for (int i = (local_filas * proceso); i < local_filas * (proceso + 1); i++)
    {
        if (arreglo[i] > valor)
        {
            valor = arreglo[i];
        }
    }
    printf("Soy el proceso %d y mi valor más alto es %d dentro de la diagonal %s\n", proceso, valor, diagonal_nombre);
    return valor;
}

void decidir_valor(int valor_alto, int valor_alto2, bool suma_valMax)
{
    sleep(0.5);
    if (suma_valMax)
    {
        if (valor_alto == valor_alto2)
        {
            printf("El valor de la suma %d es igual en ambas diagonales\n", valor_alto);
        }
        else
        {
            if (valor_alto > valor_alto2)
            {
                printf("El valor de la suma es %d, resultante de la diagonal principal\n", valor_alto);
            }
            else
            {
                printf("El valor de la suma es %d, resultante de la diagonal secundaria\n", valor_alto2);
            }
        }
    }
    else
    {
        if (valor_alto == valor_alto2)
        {
            printf("El valor más alto %d es igual en ambas diagonales\n", valor_alto);
        }
        else
        {
            if (valor_alto > valor_alto2)
            {
                printf("El valor más alto %d pertenece a la diagonal principal\n", valor_alto);
            }
            else
            {
                printf("El valor más alto %d pertenece a la diagonal secundaria\n", valor_alto2);
            }
        }
    }
}
